package firstproject;

public class Saving extends Account {
  public void display() {
	  minimumbalance =0;
	  description="A savings account is an interest-bearing deposit account held at a bank"
	  		+ " or other financial institution. \nThough these accounts typically pay a modest "
	  		+ "interest rate, their safety and reliability make them \na"
	  		+ " great option for parking cash you want available for short-term needs.";
	  System.out.println("Accountid is:" +accountid);
	  System.out.println("Description is: "+description);
	  System.out.println("Minimum Balance for this account is: "+minimumbalance);
  }
}
