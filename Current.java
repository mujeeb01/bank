package firstproject;

public class Current extends Account {
	
  public void display() {
	 
	  minimumbalance =500;
	  description="The current account records a nation's transactions "
	  		+ "with the rest of the world—specifically \nits net trade in goods "
	  		+ "and services, its net earnings on cross-border investments, \nand its "
	  		+ "net transfer payments—over a defined period, such as a year or a quarter.";
	  
	  System.out.println("Accountid is:" +accountid);
	  System.out.println("Description is: "+description);
	  System.out.println("Minimum Balance for this account is: "+minimumbalance);
  }
}
